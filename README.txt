CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration

INTRODUCTION
------------
The drupal core filter "Convert URLs into links" (filter_url) is converting strings
with "@" into email addresses even if there is no domain suffix present.
 (eg: working@home).
While those strings contain valid domain name (home is a fully valid domain name
 by standard) with the current usage of "@" character this might not be the
  desired effect. (this is the topic of the ticket https://www.drupal.org/node/2016739

This module contains a copy of the core filter filter_url with afox patch
 on #2016739 (https://www.drupal.org/node/2016739#comment-7757169).

This sandbox module is intended to be used until #2016739 is commited
and back-ported in D7 and if users don't want to patch the core.

CONFIGURATION
-------------

    1. Visit the Text format admin page (admin/config/content/formats)
    2. Click configure on each text format
    3. Everytime you have "Convert URLs into links" ticked (in Enabled filters
     section) untick it and tick "Convert URLs into links EXCEPT string link foo@bar"
    4. click save configuration
